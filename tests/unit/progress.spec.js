class Progress {
  constructor() {
    this.value = 0;
  }

  increment(incremental = 1) {
    this.value += incremental;
    if (this.value > 100) {
      this.value = 100;
    }
  }

  canIncrement() {
    if (this.value < 100) {
      return true;
    }
    return false;
  }
}

describe('Progress', () => {
  describe('increment', () => {
    test('進捗は最初は 0', () => {
      const target = new Progress();

      expect(target.value).toEqual(0);
    });

    test('進捗を進める', () => {
      const target = new Progress();

      target.increment();

      expect(target.value).toEqual(1);
    });

    test('進捗を規定値30回進める', () => {
      const target = new Progress();

      for (let i = 0; i < 30; i += 1) {
        target.increment();
      }

      expect(target.value).toEqual(30);
    });

    test('進捗を20進める', () => {
      const target = new Progress();

      target.increment(20);

      expect(target.value).toEqual(20);
    });

    test('100を超えない', () => {
      const target = new Progress();

      target.increment(60);
      target.increment(60);

      expect(target.value).toEqual(100);
    });
  });

  describe('canIncrement', () => {
    test('100未満で進捗を進められる', () => {
      const target = new Progress();
      target.increment(99);

      expect(target.value).toBeTruthy();
    });

    test('100以上は進捗を進められない', () => {
      const target = new Progress();
      target.increment(100);

      expect(target.value).toBeTruthy();
    });
  });
});
